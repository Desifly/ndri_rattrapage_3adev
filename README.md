# README #

### Informations ###

Vous pouvez trouver l'adresse de production ici : http://dndri.eemi.tech/rattrapage/prod/

Je n'ai pas réussi à déclencher les triggers dans MySql, la console me donnait une erreur de syntaxe systématiquement :
	
	create trigger adding_user
	before insert in newsletter
	for each row begin
	
	insert into newsletter_audit
	set trigger_action = 'add',
	trigger_date = now(),
	user_email = old.user_email
	
	end
	

J'ai préféré passer à la suite et intégrer une methode au moment du register pour simuler l'effet du trigger de création.