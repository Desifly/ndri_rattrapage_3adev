<?php
class coreController extends core
{
	protected $load;
	protected $model;

	function __construct($module)
	{
		$this->load = new coreView();

		include_once('app/model/'.$module.'.php');
		$this->model = new Model();

		if(isset($_GET["action"])){

			$action = $_GET["action"];
			if(method_exists($this, $action)){
					$this->$action();
			}else{
				$this->page404();
			}

		}else{
			$this->index(5,0);
		}

	}
}
