<?php
class coreModel extends core
{

  function __construct()
  {
    try
    {
      $dns = 'mysql:host='.DB_HOST.';dbname='.DB_NAME;
      $utilisateur = DB_USER;
      $motDePasse = DB_PASSWORD;

      $option = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

        $this->pdo = new PDO ( $dns, $utilisateur, $motDePasse, $option );
      }
      catch(Exception $e)
      {
        $this->coreDbError($e);
      }
    }

    protected function coreDbError($e){
      if(defined('DEBUG') && DEBUG){
        $message = SITE_NAME . " dit : erreur Mysql : " .$e->getMessage();
      }else{
        $message = SITE_NAME . " dit : Désolé une erreur technique à eu lieu";
      }

      echo $message;
      exit;
    }
  }
