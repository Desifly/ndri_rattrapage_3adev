<?php

    // load des params
    include_once('app/config/server.php');
    include_once('app/config/config.inc.php');
    include_once('app/config/db.inc.php');

    // bug test
    if (defined('DEBUG') && DEBUG) {
        ini_set('display_error', 1);
        error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
    } else {
        ini_set('display_error', 0);
        error_reporting(0);
    }

    // load du core
    include_once('core/core.php');
    include_once('core/coreController.php');
    include_once('core/coreModel.php');
    include_once('core/coreView.php');

    include_once('app/appController.php');
    include_once('app/appModel.php');

    // run app
    include_once('app/app.php');
