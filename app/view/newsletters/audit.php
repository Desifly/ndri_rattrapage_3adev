<?php
include 'app/view/layout/header.inc.php';
?>

<div class="bloc">
    <h2>Liste des inscriptions</h2>
    <?php foreach ($data as $key => $value) {
        echo '<p>L\'utilisateur '.$value["user_email"].' s\'est inscris le '.$value["trigger_date"].'</p>';
    }
    ?>
    <a class="waves-effect waves-light btn blue" href="?action=index">Retour à l'accueil</a>
</div>
<?php
include 'app/view/layout/footer.inc.php';
?>
