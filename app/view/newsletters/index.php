<?php
include 'app/view/layout/header.inc.php';
?>

    <div class="bloc">
        <form action="?action=register" method="post">
            <input type="email" name="email" value="Entrez votre mail ici">
            <input type="submit" class="waves-effect waves-light btn" value="S'enregistrer">
        </form>
          <h2>Liste des abonnés</h2>
          <?php foreach ($data as $key => $value) {
            echo '<p>'.$value["user_email"].'</p>';
            echo '<a class="waves-effect waves-light btn red" href="?action=delete&id='.$value["ID"].'"> Désabonnement </a>';
          }
          ?>
        </div>
        <div>
            <a class="waves-effect waves-light btn blue" href="?action=audit">Aller à la page d'audit</a>
        </div>
    <?php
include 'app/view/layout/footer.inc.php';
?>
