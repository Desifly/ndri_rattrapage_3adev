<?php
class Controller extends appController{

    function index()
    {

        $data = $this->model->newsletterList();

        if ($data){
            $this->load->view('newsletters', 'index.php', $data);
        }
        else{
            $this->page404();
        }

    }

    function audit()
    {

        $data = $this->model->auditList();

        if ($data){
            $this->load->view('newsletters', 'audit.php', $data);
        }
        else{
            $this->page404();
        }

    }


    function register()
    {
        if(isset($_POST["email"])){
            $data = $this->model->register($_POST["email"]);
            $this->model->registerAudit('add', $_POST['email']);
            if ($data){
                $this->index();
            }
            else{
                $this->page404();
            }
        }
    }

    function delete()
    {
        if(isset($_GET["id"])){
            $id=$_GET["id"];
            $data = $this->model->delete($id);
            if ($data){
                $this->index();
            }
            else{
                $this->page404();
            }
        }
    }
}
