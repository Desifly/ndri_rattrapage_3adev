<?php

// Récupération du param de l'url
if (!isset($_GET['module'])) {
    $module = DEFAULT_MODULE;
} else {
    $module = $_GET['module'];
}

// Appel du controleur du module demandé
$controller = 'app/controller/'.$module.'s.php';
if (file_exists($controller)) {
    include_once($controller);

    // Instanciation du controleur
    new Controller($module);
} else {
    include_once('app/view/layout/404.php');
}
