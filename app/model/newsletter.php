<?php

class Model extends appModel
{

		function newsletterList()
		{
			try{

				// on constitue la requête
				$query = $this->pdo->prepare('SELECT * FROM newsletter');

				// Execution de la requête
				$query->execute();

				// Récupération des résultats
                $newsletters = $query->fetchAll();

				// Suppression du curseur
				$query->closeCursor();
				// On retourne les résultats
				return $newsletters;
			}
			catch (Exception $e){
				return false;
			}
		}

    function auditList()
    {
        try{

            // on constitue la requête
            $query = $this->pdo->prepare('SELECT * FROM newsletter_audit');

            // Execution de la requête
            $query->execute();

            // Récupération des résultats
            $audits = $query->fetchAll();

            // Suppression du curseur
            $query->closeCursor();
            // On retourne les résultats
            return $audits;
        }
        catch (Exception $e){
            return false;
        }
    }

		function register($email)
		{
			try{

				// On constitue la requête
				$query = $this->pdo->prepare('INSERT INTO newsletter (user_email) VALUES (:user_email)');

				// Initialisation des paramètres
				$query->bindParam(':user_email', $email, PDO::PARAM_STR);

				// Execution de la requête
				$query->execute();

				// Suppression du curseur
				$query->closeCursor();

				// On retourne les résultats
				return true;
			}
			catch (Exception $e){
				return false;
			}
		}

    function registerAudit($triggerAction, $email)
    {
        try{

            // On constitue la requête
            $query = $this->pdo->prepare('INSERT INTO newsletter_audit (trigger_action, trigger_date, user_email) 
                                          VALUES (:trigger_action, now(), :user_email)');

            // Initialisation des paramètres
            $query->bindParam(':trigger_action', $triggerAction, PDO::PARAM_STR);
            $query->bindParam(':user_email', $email, PDO::PARAM_STR);

            // Execution de la requête
            $query->execute();

            // Suppression du curseur
            $query->closeCursor();

            // On retourne les résultats
            return true;
        }
        catch (Exception $e){
            return false;
        }
    }

		function delete($id)
		{
			try{

				// On constitue la requête
				$query = $this->pdo->prepare('DELETE FROM newsletter
				where ID = :id');

				// Initialisation des paramètres
				$query->bindParam(':id', $id, PDO::PARAM_INT);

				// Execution de la requête
				$query->execute();

				// Suppression du curseur
				$query->closeCursor();

				// On retourne les résultats
				return true;
			}
			catch (Exception $e){
				return false;
			}
		}


		function newsletterRead($id)
		{
			try{


				// On constitue la requête
				$query = $this->pdo->prepare('SELECT * FROM newsletter WHERE ID = :id');

				// Initialisation des paramètres
				$query->bindParam(':id', $id, PDO::PARAM_INT);

				// Execution de la requête
				$query->execute();

				// Récupération des résultats
				$newsletter = $query->fetch();

				// Suppression du curseur
				$query->closeCursor();

				// On retourne les résultats
				return $newsletter;
			}
			catch (Exception $e){
				return false;
			}
		}
	}
