<?php
define("DEFAULT_MODULE", "newsletter");

if(SERVER === "DEV"){

  define('DEBUG', true);
  define('RUN', 'NORMAL');
  define('GA', false);

} else if(SERVER === "TEST"){

  define('DEBUG', true);
  define('RUN', 'NORMAL');
  define('GA', false);

} else if(SERVER === "PROD"){

  define('DEBUG', true);
  define('RUN', 'WAIT');
  define('GA', true);

}
